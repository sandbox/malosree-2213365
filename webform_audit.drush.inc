<?php
/**
 * @file
 * Contains the drush commands created by the Webform Audit module.
 */

/**
 * Implements hook_drush_help().
 */
function webform_audit_drush_help($command) {
  switch ($command) {
    case 'drush:not-webforms':
      return dt('Returns information about webforms that have subsequently been changed to other content types.');

    case 'drush:check-from-address':
      return dt('Check for webforms with from_address configured incorrectly.');

    case 'drush:search-webform-fields':
      return dt('Searches for the given string in the names and descriptions of webform fields.');
  }
}

/**
 * Implements hook_drush_command().
 */
function webform_audit_drush_command() {
  $items = array();
  $items['not-webforms'] = array(
    'description' => dt('Returns information about webforms that have subsequently been changed to other content types.'),
    'examples' => array(
      'Standard example' => 'drush not-webforms',
    ),
    'aliases' => array('nwf'),
  );
  $items['check-from-address'] = array(
    'description' => dt('Check for webforms with from_address configured incorrectly.'),
    'options' => array(
      'non-email' => dt('List webforms with from_address set to a non-email component.'),
      'default' => dt('List webforms with from_address set to default.'),
      'other' => dt('List webforms with from_address set to some fixed value other than default.'),
      'optional' => dt('List webforms with optional from_address.'),
      'excludelist' => dt('Comma separated list of nids to exclude.'),
      'excludefile' => dt('Name of file with list of nids to exclude.'
      ),
    ),

    'examples' => array(
      'Standard example' => 'drush check-from-address --default --non-email',
      'Excludes example' => 'drush check-from-address --default --excludefile=excluded_nids.txt',
    ),

    'aliases' => array('cfa'),
  );

  $items['search-webform-fields'] = array(
    'description' => dt('Searches for the given words in the names and descriptions of webform fields.'),
    'examples' => array(
      'Wordslist example' => 'drush search-webform-fields --wordslist=a,b,c,d',
      'Wordsfile example' => 'drush search-webform-fields --wordsfile=wordsfile.txt',
      'Excludes example' => 'drush search-webform-fields --wordslist=a,b,c,d --excludelist=1,2,3,4',
    ),

    'options' => array(
      'wordsfile' => dt('Name of file with list of words to look for.'),
      'wordslist' => dt('Comma separated list of words to look for.'),
      'excludelist' => dt('Comma separated list of nids to exclude.'),
      'excludefile' => dt('Name of file with nids to exclude.'),
    ),
    'aliases' => array('swf'),
  );
  return $items;
}

/**
 * Callback function for drush not-webforms.
 */
function drush_webform_audit_not_webforms() {
  $result = db_query("SELECT node.nid, node.title, node.type from {node}, {webform} where node.nid=webform.nid and node.type!='webform'");
  drush_print(dt('Printing information about webforms whose node type has been changed:'));
  foreach ($result as $res) {
    drush_print(dt("nid: !nid \ttitle: !title \ttype: !type", array(
    '!nid' => $res->nid,
    '!title' => $res->title,
    '!type' => $res->type)
    ));
  }
}


/**
 * Callback function for drush check-from-address.
 */
function drush_webform_audit_check_from_address() {
  drush_print(dt("Results for webforms with problems in the from_address:"));
  $non_email = drush_get_option('non-email');
  $default = drush_get_option('default');
  $other = drush_get_option('other');
  $optional = drush_get_option('optional');
  $excludelist = drush_get_option('excludelist');
  $excludefile = drush_get_option('excludefile');

  $exclude_list = array();
  $exclude_list = array_merge($exclude_list, explode(',', $excludelist));
  if ($excludefile) {
    if (file_get_contents($excludefile)) {
      $exclude_list = array_merge($exclude_list, explode(',', file_get_contents($excludefile)));
    }
    else {
      return drush_set_error('EMPTY_FILE', dt('The excludefile could not be loaded or was empty. The command could not be run.'));
    }
  }

  if (!($non_email||$default||$other||$optional)) {
    $all = TRUE;
  }

  $problems = array();

  if ($non_email||$all) {
    drush_log(dt('Checking for webfrorms with field of type other than email as from_address.'), 'notice');
    $result = db_query("SELECT webform_emails.nid, node.title, email, cid, name, webform_component.type, from_address from {webform_component}, {webform_emails}, {node} where from_address=cid and webform_component.nid=webform_emails.nid and webform_component.type!='email' and node.nid=webform_emails.nid");
    foreach ($result as $res) {
      if (!in_array($res->nid, $exclude_list)) {
        array_push($problems, "Non-email- nid: " . $res->nid . " title: " . $res->title . " email: " . $res->email . " cid: " . $res->cid . " name: " . $res->name . " type: " . $res->type);
      }
    }
  }

  if ($default||$all) {
    drush_log(dt('Checking for webforms with "default" as from_address.'), 'notice');
    $result = db_query("SELECT node.nid, title, email FROM {webform_emails}, {node} WHERE from_address='default' and node.nid=webform_emails.nid");
    foreach ($result as $res) {
      if (!in_array($res->nid, $exclude_list)) {
        array_push($problems, "Default- nid: " . $res->nid . " title: " . $res->title . " email: " . $res->email);
      }
    }
  }

  if ($other||$all) {
    drush_log(dt('Checking for webforms with something other than "default" or a component number as from_address.'), 'notice');
    $result = db_query("SELECT node.nid, title, email, from_address FROM {webform_emails}, {node} WHERE node.nid=webform_emails.nid AND from_address not REGEXP '[0-9]+' AND from_address!='default'");
    foreach ($result as $res) {
      if (!in_array($res->nid, $exclude_list)) {
        array_push($problems, "Other- nid: " . $res->nid . " title: " . $res->title . " email: " . $res->email . " from_address: " . $res->from_address);
      }
    }
  }

  if ($optional||$all) {
    drush_log(dt('Checking for webfrorms with optional from_address.'), 'notice');
    $result = db_query("SELECT webform_emails.nid, node.title, cid, name, webform_emails.email from {webform_component}, {webform_emails}, {node} where from_address=cid and webform_component.nid=webform_emails.nid and webform_component.type='email' and webform_component.mandatory=0 and node.nid=webform_emails.nid");
    foreach ($result as $res) {
      if (!in_array($res->nid, $exclude_list)) {
        array_push($problems, "Optional- nid: " . $res->nid . " title: " . $res->title . " email: " . $res->email . " cid: " . $res->cid . " name: " . $res->name);
      }
    }
  }

  if (!empty($problems)) {
    foreach ($problems as $problem) {
      drush_print(dt($problem));
    }
  }
  else {
    drush_print(dt("No results."));
  }

}

/**
 * Callback function for drush search-webform-fields.
 */
function drush_webform_audit_search_webform_fields() {
  $wordsfile = drush_get_option('wordsfile');
  $wordslist = drush_get_option('wordslist');
  $excludelist = drush_get_option('excludelist');
  $excludefile = drush_get_option('excludefile');

  $exclude_list = array();
  $exclude_list = array_merge($exclude_list, explode(',', $excludelist));
  if ($excludefile) {
    if (file_get_contents($excludefile)) {
      $exclude_list = array_merge($exclude_list, explode(',', file_get_contents($excludefile)));
    }
    else {
      return drush_set_error('EMPTY_FILE', dt('The excludefile could not be loaded or was empty. The command could not be run.'));
    }
  }

  if (!$wordsfile & !$wordslist) {
    return drush_set_error('OPTION_REQUIRED', dt('Option wordsfile or option wordslist or both must be provided.'));
  }

  else {
    $matches = array();
    $word_list = array();
    if ($wordsfile) {
      if (file_get_contents($wordsfile)) {
        $word_list = array_merge($word_list, explode(',', file_get_contents($wordsfile)));
      }
      else {
        return drush_set_error('EMPTY_FILE', dt('The wordsfile could not be loaded or was empty. The command could not be run.'));
      }
    }
    if ($wordslist) {
      $word_list = array_merge($word_list, explode(',', $wordslist));
    }
    foreach ($word_list as $word) {
      drush_log(dt('Looking for "!word".', array('!word' => $word)), 'notice');
      $result = db_query("SELECT nid FROM {webform}");
      foreach ($result as $res) {
        if (!in_array($res->nid, $exclude_list)) {
          $matches = array_merge($matches, webform_audit_checkfields($res->nid, trim($word)));
        }
      }
    }
    drush_print(dt("Results for webforms with forbidden words:"));
    if (!empty($matches)) {
      foreach ($matches as $match) {
        drush_print(dt($match));
      }
    }
    else {
      drush_print(dt("No results."));
    }
  }

}

/**
 * Searches the fields of webforms. 
 *
 * Called from drush_webform_audit_search_webform_fields.
 */
function webform_audit_checkfields($form_nid, $word) {
  $result = db_query("SELECT node.nid, cid, title, form_key, name, webform_component.type FROM {webform_component},{node} WHERE webform_component.nid = :form_nid AND node.nid= :form_nid", array(':form_nid' => $form_nid));
  $matches = array();
  foreach ($result as $res) {
    if (stripos($res->form_key, $word) !== FALSE || stripos($res->name, $word) !== FALSE) {
      array_push($matches, "Contains " . $word . " nid: " . $res->nid . "  title: " . $res->title . "  cid: " . $res->cid . " form_key: " . $res->form_key . " name: " . $res->name . " type: " . $res->type);
    }
  }
  return $matches;
}
